var express = require('express');
var router = express.Router();

const venueContoller = require('../controllers/venues')


router.post('/',venueContoller.store)
router.get('/',venueContoller.findAll)
router.get('/:id',venueContoller.show)
router.put('/:id',venueContoller.update)
router.delete('/:id',venueContoller.destroy)

module.exports = router;