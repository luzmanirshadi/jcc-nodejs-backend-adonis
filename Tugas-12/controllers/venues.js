const {Venue} = require('../models')

class venueController{
    static async findAll(req,res){
        let venues = await Venue.findAll()
        console.log(venues)
        res.status(200).json({status: 'success',data: venues})
    }

    static async store(req,res){
        let name = req.body.name
        let address = req.body.address
        let phone = req.body.phone
        await Venue.create({name,address,phone})
        res.status(201).json({status: 'success',message:'venue is saved'})
    }

    static async show(req,res){
        let id = req.params.id
        let venue1 = await Venue.findByPk(id)
        res.status(200).json({status:"success",data: venue1})
    }

    static async update(req,res){
        let name = req.body.name
        let address = req.body.address
        let phone = req.body.phone
        await Venue.update({
            name,
            address,
            phone
        },{
            where:{
                id: req.params.id
            }
        })
        res.status(200).json({status:"success",data:"updated"})
    }

    static async destroy(req,res){
        await Venue.destroy({
            where: {
                id:req.params.id
            }
        })
        res.status(200).json({status:"success",data:"deleted"})
    }
}

module.exports = venueController