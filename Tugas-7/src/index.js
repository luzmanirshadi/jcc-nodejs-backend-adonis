import Bootcamp from './lib/bootcamp'
import Student from './lib/student'

const sanber = new Bootcamp("sanbercode")

sanber.createClass("Laravel", "beginner", "abduh")

sanber.createClass("React", "beginner", "abdul")

let names = ["regi", "ahmad", "bondra", "iqbal", "putri", "rezky"]
names.map((nama, index) => {
  let newStud = new Student(nama)
  let kelas = index%2
  sanber.register(kelas, newStud)
})

sanber.runBatch()

sanber.classes.forEach(kelas => {
        kelas.graduated()
    });