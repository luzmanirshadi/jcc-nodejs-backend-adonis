class Student{
    constructor(nama){
        this._nama = nama
        this._score = []
        this._finalscore = 0
    }

    get nama(){
        return this._nama
    }

    get score(){
        return this._score
    }

    get finalscore(){
        return this._finalscore
    }

    set nama(nama){
        this._nama = nama
    }

    sumScore(){
        let sum = 0
        for(let i = 0;i < this._score.length;i++)
        {
            sum += this._score[i]
        }
        return sum
    }

    assignScore(score){
        this._score.push(score)
        this._finalscore = Math.floor(this.sumScore()/this._score.length)
    }
}

export default Student