
class Kelas{
    constructor(nama,level,instructor){
        this._nama = nama
        this._students = []
        this._level = level
        this._instructor = instructor
    }

    get nama(){
        return this._nama
    }

    get students(){
        return this._students
    }

    get instructor(){
        return this._instructor
    }

    get level(){
        return this._level
    }

    set nama(nama){
        this._nama = nama
    }

    set instructor(instructor){
        this._instructor = instructor
    }

    set level(level){
        this._level = level
    }

    assignStudent(student){
        this._students.push(student)
    }

    graduate(){
        var graduate = {}
        var participant = []
        var completed = []
        var mastered = []
        for(let i = 0;i < this._students.length;i++)
        {
            if(this._students[i].finalscore < 60)
            {
                participant.push(this._students[i])
            }
            else if(this._students[i].finalscore < 85)
            {
                completed.push(this._students[i])
            }
            else
            {
                mastered.push(this._students[i])
            }
        }
        graduate.participant = participant
        graduate.completed = completed
        graduate.mastered = mastered
        return graduate
    }

    graduated(){
        var graduate = this.graduate()
        console.log(`graduated from ${this._nama}:`,graduate)
    }
}

export default Kelas