import Kelas from './class'
import Student from './class'

class Bootcamp {
    constructor(nama){
        this._nama = nama
        this._classes = []
    }

    get nama(){
        return this._nama
    }

    get classes(){
        return this._classes
    }

    set nama(nama){
        this._nama = nama
    }

    createClass(nama,level,instuctor){
        var kelasBaru = new Kelas(nama,level,instuctor)
        this._classes.push(kelasBaru)
    }

    register(kelas,student){
        this._classes[kelas].assignStudent(student)
    }

    randomScore(min,max){
        return Math.floor(
            Math.random() * (max - min) + min
          )
    }

    runBatch(){
        for(let i = 0;i < 4;i++)
        {
            this._classes.forEach(kelas =>{
                kelas.students.forEach(student =>{
                    student.assignScore(this.randomScore(50,100))
                })
            })
        }
    }
}

export default Bootcamp