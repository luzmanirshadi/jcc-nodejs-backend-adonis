"use strict";

var _bootcamp = _interopRequireDefault(require("./lib/bootcamp"));

var _student = _interopRequireDefault(require("./lib/student"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var sanber = new _bootcamp["default"]("sanbercode");
sanber.createClass("Laravel", "beginner", "abduh");
sanber.createClass("React", "beginner", "abdul");
var names = ["regi", "ahmad", "bondra", "iqbal", "putri", "rezky"];
names.map(function (nama, index) {
  var newStud = new _student["default"](nama);
  var kelas = index % 2;
  sanber.register(kelas, newStud);
});
sanber.runBatch();
sanber.classes.forEach(function (kelas) {
  kelas.graduated();
});