"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _class = _interopRequireDefault(require("./class"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Bootcamp = /*#__PURE__*/function () {
  function Bootcamp(nama) {
    _classCallCheck(this, Bootcamp);

    this._nama = nama;
    this._classes = [];
  }

  _createClass(Bootcamp, [{
    key: "nama",
    get: function get() {
      return this._nama;
    },
    set: function set(nama) {
      this._nama = nama;
    }
  }, {
    key: "classes",
    get: function get() {
      return this._classes;
    }
  }, {
    key: "createClass",
    value: function createClass(nama, level, instuctor) {
      var kelasBaru = new _class["default"](nama, level, instuctor);

      this._classes.push(kelasBaru);
    }
  }, {
    key: "register",
    value: function register(kelas, student) {
      this._classes[kelas].assignStudent(student);
    }
  }, {
    key: "randomScore",
    value: function randomScore(min, max) {
      return Math.floor(Math.random() * (max - min) + min);
    }
  }, {
    key: "runBatch",
    value: function runBatch() {
      var _this = this;

      for (var i = 0; i < 4; i++) {
        this._classes.forEach(function (kelas) {
          kelas.students.forEach(function (student) {
            student.assignScore(_this.randomScore(50, 100));
          });
        });
      }
    }
  }]);

  return Bootcamp;
}();

var _default = Bootcamp;
exports["default"] = _default;