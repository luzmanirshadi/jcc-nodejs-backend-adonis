"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Student = /*#__PURE__*/function () {
  function Student(nama) {
    _classCallCheck(this, Student);

    this._nama = nama;
    this._score = [];
    this._finalscore = 0;
  }

  _createClass(Student, [{
    key: "nama",
    get: function get() {
      return this._nama;
    },
    set: function set(nama) {
      this._nama = nama;
    }
  }, {
    key: "score",
    get: function get() {
      return this._score;
    }
  }, {
    key: "finalscore",
    get: function get() {
      return this._finalscore;
    }
  }, {
    key: "sumScore",
    value: function sumScore() {
      var sum = 0;

      for (var i = 0; i < this._score.length; i++) {
        sum += this._score[i];
      }

      return sum;
    }
  }, {
    key: "assignScore",
    value: function assignScore(score) {
      this._score.push(score);

      this._finalscore = Math.floor(this.sumScore() / this._score.length);
    }
  }]);

  return Student;
}();

var _default = Student;
exports["default"] = _default;