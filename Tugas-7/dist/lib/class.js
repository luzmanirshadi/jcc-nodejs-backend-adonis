"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Kelas = /*#__PURE__*/function () {
  function Kelas(nama, level, instructor) {
    _classCallCheck(this, Kelas);

    this._nama = nama;
    this._students = [];
    this._level = level;
    this._instructor = instructor;
  }

  _createClass(Kelas, [{
    key: "nama",
    get: function get() {
      return this._nama;
    },
    set: function set(nama) {
      this._nama = nama;
    }
  }, {
    key: "students",
    get: function get() {
      return this._students;
    }
  }, {
    key: "instructor",
    get: function get() {
      return this._instructor;
    },
    set: function set(instructor) {
      this._instructor = instructor;
    }
  }, {
    key: "level",
    get: function get() {
      return this._level;
    },
    set: function set(level) {
      this._level = level;
    }
  }, {
    key: "assignStudent",
    value: function assignStudent(student) {
      this._students.push(student);
    }
  }, {
    key: "graduate",
    value: function graduate() {
      var graduate = {};
      var participant = [];
      var completed = [];
      var mastered = [];

      for (var i = 0; i < this._students.length; i++) {
        if (this._students[i].finalscore < 60) {
          participant.push(this._students[i]);
        } else if (this._students[i].finalscore < 85) {
          completed.push(this._students[i]);
        } else {
          mastered.push(this._students[i]);
        }
      }

      graduate.participant = participant;
      graduate.completed = completed;
      graduate.mastered = mastered;
      return graduate;
    }
  }, {
    key: "graduated",
    value: function graduated() {
      var graduate = this.graduate();
      console.log("graduated from ".concat(this._nama, ":"), graduate);
    }
  }]);

  return Kelas;
}();

var _default = Kelas;
exports["default"] = _default;