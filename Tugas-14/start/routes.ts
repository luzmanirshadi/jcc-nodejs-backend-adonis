/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

Route.resource('venues.fields','FieldsController').middleware({'*':['auth']})
Route.resource('venues','VenuesController').middleware({'*':['auth']})
Route.post('/bookings','BookingsController.store').as('booking.store').middleware(['auth'])
Route.post('/register','AuthController.register').as('auth.register')
Route.post('/login','AuthController.login').as('auth.login')
Route.get('/profiles','ProfilesController.index').as('profile.index').middleware(['auth'])
Route.post('/profiles','ProfilesController.store').as('profile.store').middleware(['auth'])