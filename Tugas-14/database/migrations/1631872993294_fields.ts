import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import {PostType} from 'Contracts/enum'

export default class Fields extends BaseSchema {
  protected tableName = 'fields'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name').notNullable()
      table.enum('type',Object.values(PostType)).defaultTo(PostType.FUTSALL)
      table.integer('venue_id').unsigned().references('id').inTable('venues')
      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
       table.timestamps(true,true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
