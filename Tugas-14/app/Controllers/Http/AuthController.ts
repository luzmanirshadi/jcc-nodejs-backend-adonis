import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateUserValidator from 'App/Validators/CreateUserValidator'
import User from 'App/Models/User'
import { schema} from '@ioc:Adonis/Core/Validator'

export default class AuthController {
    public async register({request,response}:HttpContextContract){
        try{
            const data = await request.validate(CreateUserValidator)
            const newUser = await User.create(data)
            return response.created({message: 'registered'})
        }
        catch(error)
        {
            return response.unprocessableEntity({messages: error.message})
        }
    }

    public async login({request,response,auth}:HttpContextContract){
        try{
            const userSchema = schema.create({
                email: schema.string(),
                password: schema.string()
            })
            const email = request.input('email')
            const password = request.input('password')

            const payload = await request.validate({schema:userSchema})
            const token = await auth.use('api').attempt(email,password)

            return response.ok({message: 'login success',token})
        }
        catch(error)
        {
            if(error.guard)
            {
                return response.badRequest({message: 'login error', error:error.message})
            }
            else
            {
                return response.badRequest({message: 'login error', error:error.messages})
            }
        }
    }
}
