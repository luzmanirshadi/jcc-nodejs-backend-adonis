import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateVenueValidator from 'App/Validators/CreateVenueValidator'
import Database from '@ioc:Adonis/Lucid/Database'
import Venue from 'App/Models/Venue'

export default class VenuesController {
    public async index({response}:HttpContextContract){
        //Query Builder
        //let venues = await Database.from('venues').select('id','name','address','phone')

        //Model ORM
        let venues = await Venue.all()

        response.ok({message : 'success get venues', data: venues})
    }

    public async store({request,response}:HttpContextContract){
        await request.validate(CreateVenueValidator)
        //Query Builder
        /*let newVenue = await Database.table('venues').returning('id').insert({
            name: request.input('name'),
            address: request.input('address'),
            phone: request.input('phone')
        })*/

        //Model ORM
        let newVenue = new Venue()
        newVenue.name = request.input('name')
        newVenue.address = request.input('address')
        newVenue.phone = request.input('phone')
        await newVenue.save()

        response.created({message: 'venue created!', newId: newVenue})
    }

    public async show({params,response}:HttpContextContract){
        //Query Builder
        //let venue = await Database.from('venues').where('id',params.id).select('id','name','address','phone').first()
        
        //Model ORM
        let venue = await Venue.query().where('id',params.id).preload('fields').first()
        
        return response.ok({message:'success get venues with id',data: venue})
    }

    public async update({params,request,response}:HttpContextContract){
        let id = params.id
        //Query builder
        /*await Database.from('venues').where('id',id).update({
            address: request.input('address'),
            phone: request.input('phone'),
            name: request.input('name')
        })*/

        //Model ORM
        let newVenue = await Venue.findOrFail(id)
        newVenue.name = request.input('name')
        newVenue.address = request.input('address')
        newVenue.phone = request.input('phone')
        await newVenue.save()

        response.ok({message : 'success update venue'})
    }

    public async destroy({params,response}:HttpContextContract){
        //Query builder
        //await Database.from('venues').where('id',params.id).delete()

        //Model ORM
        let venue = await Venue.findOrFail(params.id)
        await venue.delete()
        response.ok({message : 'success delete venue'})
    }
}
