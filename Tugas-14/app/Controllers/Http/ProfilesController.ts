import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateProfileValidator from 'App/Validators/CreateProfileValidator'
import Database from '@ioc:Adonis/Lucid/Database'
import Profile from 'App/Models/Profile'

export default class ProfilesController {
    public async index({response}:HttpContextContract){
        //Query Builder
        //let venues = await Database.from('venues').select('id','name','address','phone')

        //Model ORM
        let profiles = await Profile.all()

        response.ok({message : 'success get profiles', data: profiles})
    }

    public async store({request,response,auth}:HttpContextContract){
        await request.validate(CreateProfileValidator)
        //Query Builder
        /*let newProfile = await Database.table('venues').returning('id').insert({
            name: request.input('name'),
            address: request.input('address'),
            phone: request.input('phone')
        })*/

        //Model ORM
        const name = request.input('name')
        const address = request.input('address')
        const phone = request.input('phone')
        //const userId = await auth.user?.id
        //let newProfile = Profile.create({
            //name: name,
            //address: address,
            //phone: phone,
            //user_id: userId
        //})

        const authUser = auth.user
        authUser?.related('profile').create({
            name: name,
            address: address,
            phone: phone
        })

        return response.created({message: 'profile created!'})
    }

    public async show({params,response}:HttpContextContract){
        //Query Builder
        //let venue = await Database.from('venues').where('id',params.id).select('id','name','address','phone').first()
        
        //Model ORM
        let profile = await Profile.find(params.id)
        
        return response.ok({message:'success get profile with id',data: profile})
    }

    public async update({params,request,response}:HttpContextContract){
        let id = params.id
        //Query builder
        /*await Database.from('venues').where('id',id).update({
            address: request.input('address'),
            phone: request.input('phone'),
            name: request.input('name')
        })*/

        //Model ORM
        let newProfile = await Profile.findOrFail(id)
        newProfile.name = request.input('name')
        newProfile.address = request.input('address')
        newProfile.phone = request.input('phone')
        await newProfile.save()

        response.ok({message : 'success update profile'})
    }

    public async destroy({params,response}:HttpContextContract){
        //Query builder
        //await Database.from('venues').where('id',params.id).delete()

        //Model ORM
        let profile = await Profile.findOrFail(params.id)
        await profile.delete()
        response.ok({message : 'success delete profile'})
    }
}
