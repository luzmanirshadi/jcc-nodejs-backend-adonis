import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateFieldValidator from 'App/Validators/CreateFieldValidator'
import Database from '@ioc:Adonis/Lucid/Database'
import Venue from 'App/Models/Venue'
import Field from 'App/Models/Field'
import { PostType } from 'Contracts/enum'

export default class FieldsController {
    public async index({response,params}:HttpContextContract){
        //Query builder
        //let fields = await Database.from('fields').where('venue_id',params.venue_id).select('id','name','type','venue_id')

        //Model ORM
        //let fields = await Field.findByOrFail('venue_id',params.id)
        let venue = await Venue.find(params.venue_id)
        if(venue?.$isPersisted)
        {
            let field = await Field.query().where('venue_id',params.venue_id)
            response.ok({message : 'success get fields',field: field})
        }
        else
        {
            response.badRequest({message : 'failed get fields'})
        }
    }

    public async store({request,response,params,auth}:HttpContextContract){
        await request.validate(CreateFieldValidator)
        //Query builder
        /*let tipe = ['futsal','mini soccer','basketball']
        let rand = Math.floor(Math.random() * (tipe.length - 0) + 0)
        console.log(rand)
        let newField = await Database.table('fields').returning('id').insert({
            name: request.input('name'),
            type: tipe[rand],
            venue_id: params.venue_id
        })*/

        //Model ORM
        if(auth.isLoggedIn)
        {
            const venue = await Venue.findByOrFail('id',params.venue_id)
            
            const newField = new Field()
            newField.name = request.input('name')
            newField.type = PostType.FUTSALL
            await newField.related('venue').associate(venue)
            //newField.save()
            return response.created({message: 'field created!'})
        }
        else
        {
            return response.badRequest({message : 'failed create fields'})
        }
    }

    public async show({params,response}:HttpContextContract){
        //Query builder
        //let field = await Database.from('fields').innerJoin('venues','venues.id','fields.venue_id').where('fields.id',params.id).select('fields.id','fields.name','fields.venue_id')
        
        //Model ORM
        let fields = await Field.query().where('venue_id',params.venue_id).andWhere('id',params.id)
        return response.ok({message:'success get fields with id',data: fields})
    }

    public async update({params,request,response}:HttpContextContract){
        //Query builder
        /*await Database.from('fields').innerJoin('venues','venues.id','fields.venue_id').where('fields.id',params.id).update({
            name: request.input('name'),
            venue_id: parseInt(request.body().venue_id)
        })*/

        //Model ORM
        let venue = await Venue.find(params.venue_id)
        if(venue?.$isPersisted)
        {
            let field = await Field.find(params.id)
            if(field?.$isPersisted && field?.venueId == venue.id)
            {
                field.name = request.input('name')
                field.type = PostType.FUTSALL
                field.venueId = request.input('venue_id')
                field.save()
                return response.ok({message : 'success update field'})
            }
        }
        return response.badRequest({message : 'failed update field'})
    }

    public async destroy({params,response}:HttpContextContract){
        //Query builder
        //await Database.from('fields').innerJoin('venues','venues.id','fields.venue_id').where('fields.id',params.id).delete()

        //Model ORM
        let venue = await Venue.find(params.venue_id)
        if(venue?.$isPersisted)
        {
            let field = await Field.find(params.id)
            if(field?.$isPersisted && field?.venueId == venue.id)
            {
                field.delete()
                return response.ok({message : 'success delete field'})
            }
        }
        return response.badRequest({message : 'failed deletefield'})
    }
}
