/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

Route.resource('venues','VenuesController').apiOnly().middleware({'*':['owner','admin']})
Route.resource('venues.fields','FieldsController').apiOnly().middleware({'*':['owner','admin']})
//Route.resource('fields.bookings','BookingsController').apiOnly().middleware({'*':['auth']})
Route.get('/fields/:id','FieldsController.new_show').as('field.new_show').middleware(['owner','admin'])
Route.post('/fields/:id/bookings','BookingsController.store').as('book.store').middleware(['user'])
Route.get('/bookings/:id','BookingsController.show').as('book.show').middleware(['admin'])
Route.put('/bookings/:id','BookingsController.join').as('book.join').middleware(['user'])
Route.post('/register','AuthController.register').as('auth.register')
Route.post('/login','AuthController.login').as('auth.login')
Route.get('/profiles','ProfilesController.index').as('profile.index').middleware(['auth'])
Route.post('/profiles','ProfilesController.store').as('profile.store').middleware(['auth'])
Route.post('/verifikasi','AuthController.confirmOTP').as('auth.confirm')
