export enum PostType {
    FUTSALL = 'futsal',
    MINISOCCER = 'mini soccer',
    BASKETBALL = 'basketball',
  }

export enum RoleType{
  VENUEOWNER = 2,
  ADMIN = 1,
  USER = 3

}