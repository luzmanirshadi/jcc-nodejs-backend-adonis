import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class UserVerify {
  public async handle ({auth,response}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    let isVerified = auth.user?.isVerified
    let role = auth.user?.role
    if(isVerified && role === 'user')
    {
      await next()
    }
    else
    {
      return response.unauthorized({message: "not verified or not authorized"})
    }
  }
}
