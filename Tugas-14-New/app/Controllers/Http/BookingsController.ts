import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field'
import User from 'App/Models/User'
import BookingValidator from 'App/Validators/BookingValidator'

export default class BookingsController {
    public async store({request,response,params,auth}:HttpContextContract){
        const payload = await request.validate(BookingValidator)
        const user = await User.find(auth.user?.id)
        const field = await Field.find(params.id)
        if(user && field)
        {
            const booking = await field.related('booking').create({
                playDateStart: payload.play_date_start,
                playDateEnd: payload.play_date_end
            })
            await user.related('bookings').attach([booking.id])
            return response.ok({status:'berhasil booking', user: user, field: field})
        }
        else
        {
            return response.badRequest({message:'field or user not exist'})
        }
    }

    public async show({request,response,params}:HttpContextContract){
        const booking = await Booking.query().where('id',params.id).preload('players', (userQuery)=>{
            userQuery.select(['email'])
        }).withCount('players').firstOrFail()
        const data = {...booking.$original, players_count : booking.$extras.players_count, players: booking.players}
        return response.ok({data: data})
    }

    public async join({request,response,params,auth}:HttpContextContract){
        const user = await User.find(auth.user?.id)
        const booking = await Booking.firstOrFail(params.id)
        if(user)
        {
            await user.related('bookings').attach([booking.id])
            return response.ok({status:'berhasil join'})
        }
        else
        {
            return response.badRequest({message:'gagal join'})
        }
    }
}
