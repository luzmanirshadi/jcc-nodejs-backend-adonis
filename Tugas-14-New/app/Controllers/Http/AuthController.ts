import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import Database from '@ioc:Adonis/Lucid/Database'
import UserValidator from 'App/Validators/UserValidator'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import Mail from '@ioc:Adonis/Addons/Mail'

export default class AuthController {
    public async register({request,response}:HttpContextContract){
        try{
            const data = await request.validate(UserValidator)
            const newUser = await User.create(data)
            const otp_code = Math.floor(100000+Math.random()*900000)

            let saveCode = await Database.table('otp_codes').insert({otp_code: otp_code, user_id: newUser.id})
            await Mail.send((message) => {
                message
                  .from('info@example.com')
                  .to(data.email)
                  .subject('Welcome Onboard!')
                  .htmlView('emails/otp_verification', {otp_code, name: 'Virk' })
              })
            return response.created({message: 'registered, please verify your otp code'})
        }
        catch(error)
        {
            return response.unprocessableEntity({messages: error.message})
        }
    }

    public async login({request,response,auth}:HttpContextContract){
        try{
            const userSchema = schema.create({
                email: schema.string(),
                password: schema.string()
            })
            const email = request.input('email')
            const password = request.input('password')

            const payload = await request.validate({schema:userSchema})
            const token = await auth.use('api').attempt(email,password)

            return response.ok({message: 'login success',token})
        }
        catch(error)
        {
            if(error.guard)
            {
                return response.badRequest({message: 'login error', error:error.message})
            }
            else
            {
                return response.badRequest({message: 'login error', error:error.messages})
            }
        }
    }

    public async confirmOTP({request,response,auth}:HttpContextContract){
        let otp_code = request.input('otp_code')
        let email = request.input('email')
        let user = await User.findByOrFail('email',email)
        let code = await Database.from('otp_codes').where('otp_code',otp_code).first()
        if(user?.id == code.user_id)
        {
            user.isVerified = true
            await user.save()
            return response.accepted({message: "verified"})
        }
        else
        {
            return response.notFound({message: "failed verify OTP"})
        }
    }
}
