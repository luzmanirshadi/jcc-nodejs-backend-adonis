import {register,login} from './lib/register'

const args = process.argv.slice(2)
const command = args[0]

switch (command) {
    case "register":
        let newData = {}
        let [nama,password,role] = args[1].split(',')
        newData.nama = nama
        newData.password = password
        newData.role = role
        newData.isLogin = false
        register(newData);
        break;
    case "login":
        let param = {}
        let [id,pass] = args[1].split(',')
        param.nama = id
        param.password = pass
        login(param);
        break;
    default:
        break;
}