import fs from 'fs'
import fspromise from 'fs/promises'
import 'core-js/stable'
import 'regenerator-runtime/runtime'

const path = 'data.json'

export const register = (newData) =>{
    console.log(newData)
    fs.readFile(path,(err,data)=>{
        if(err)console.error(err)
        let dataArr = JSON.parse(data)
        dataArr.push(newData)
        fs.writeFile(path,JSON.stringify(dataArr),{encoding:'utf-8'},(err) =>{
            if(err)console.log(err)
        })
    })
}

export const login = async (param) =>{
    let dataRead = await fspromise.readFile(path)
    let realData = JSON.parse(dataRead)
    let index = realData.findIndex(item => item.nama == param.nama && item.password == param.password)
    realData[index].isLogin = true
    await fspromise.writeFile(path,JSON.stringify(realData))
    console.log("berhasil login")
}