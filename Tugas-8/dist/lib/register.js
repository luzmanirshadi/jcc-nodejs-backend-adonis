"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.login = exports.register = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _promises = _interopRequireDefault(require("fs/promises"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var path = 'data.json';

var register = function register(newData) {
  console.log(newData);

  _fs["default"].readFile(path, function (err, data) {
    if (err) console.error(err);
    var dataArr = JSON.parse(data);
    dataArr.push(newData);

    _fs["default"].writeFile(path, JSON.stringify(dataArr), {
      encoding: 'utf-8'
    }, function (err) {
      if (err) console.log(err);
    });
  });
};

exports.register = register;

var login = function login(param) {
  _fs["default"].readFile(path, function (err, data) {
    if (err) console.error(err);
    var dataArr = JSON.parse(data);
    var indexGet = dataArr.findIndex(function (person) {
      return person.nama == param.nama && person.password == param.password;
    });

    if (indexGet != -1) {
      dataArr[indexGet].isLogin = true;

      _fs["default"].writeFile(path, JSON.stringify(dataArr), {
        encoding: 'utf-8'
      }, function (err) {
        if (err) console.log(err);
        console.log("Berhasil Login");
      });
    }
  });
};

exports.login = login;