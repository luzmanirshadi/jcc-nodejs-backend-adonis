"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.login = exports.register = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _promises = _interopRequireDefault(require("fs/promises"));

require("core-js/stable");

require("regenerator-runtime/runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var path = 'data.json';

var register = function register(newData) {
  console.log(newData);

  _fs["default"].readFile(path, function (err, data) {
    if (err) console.error(err);
    var dataArr = JSON.parse(data);
    dataArr.push(newData);

    _fs["default"].writeFile(path, JSON.stringify(dataArr), {
      encoding: 'utf-8'
    }, function (err) {
      if (err) console.log(err);
    });
  });
};

exports.register = register;

var login = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(param) {
    var dataRead, realData, index;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return _promises["default"].readFile(path);

          case 2:
            dataRead = _context.sent;
            realData = JSON.parse(dataRead);
            index = realData.findIndex(function (item) {
              return item.nama == param.nama && item.password == param.password;
            });
            realData[index].isLogin = true;
            _context.next = 8;
            return _promises["default"].writeFile(path, JSON.stringify(realData));

          case 8:
            console.log("berhasil login");

          case 9:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function login(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.login = login;