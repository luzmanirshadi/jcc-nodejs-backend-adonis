var express = require('express');
var router = express.Router();

const UserController = require('../controllers/users')

router.post('/register',UserController.register)

router.post('/login',UserController.login)

router.post('/karyawan/:name/siswa',UserController.siswa)

router.get('/karyawan',UserController.karyawan)

module.exports = router;
