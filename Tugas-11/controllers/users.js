const fs = require('fs')

class userController{
    static register(req,res){
        fs.readFile('data.json',(err,data) =>{
            if(err){
                res.status(400).json({"errors":"error membaca data karyawan"})
            }
            else{
                let existingData = JSON.parse(data)
                let {users} = existingData
                let {name,password,role} = req.body
                let newUser = {name,password,role,"isLogin":false}
                let index = users.findIndex(item => item.role == 'admin')
                if(newUser.role == 'admin' && index == -1)
                {
                    users.push(newUser)
                    let newData = {...existingData,users: users}
                    fs.writeFile('data.json',JSON.stringify(newData),(err)=>{
                        if(err)
                        {
                            res.status(400).json({errors:"error menyimpan data karyawan"})
                        }
                        else{
                            res.status(201).json({message:"berhasil register karyawan"})
                        }
                    })
                }
                else if(newUser.role == 'admin' && index != -1)
                {
                    res.status(201).json({message:"gagal menyimpan data karyawan, sudah ada admin terdaftar"})
                }
                else
                {
                    users.push(newUser)
                    let newData = {...existingData,users: users}
                    fs.writeFile('data.json',JSON.stringify(newData),(err)=>{
                        if(err)
                        {
                            res.status(400).json({errors:"error menyimpan data karyawan"})
                        }
                        else{
                            res.status(201).json({message:"berhasil register karyawan"})
                        }
                    })
                }
            }
        })
    }

    static karyawan(req,res){
        fs.readFile('data.json',(err,data) =>{
            if(err){
                res.status(400).json({"errors":"error membaca data karyawan"})
            }
            else{
                let realData = JSON.parse(data)
                res.status(200).json({message:"berhasil get data karyawan",data: realData.users})
            }
        })
    }

    static login(req,res){
        fs.readFile('data.json',(err,data) =>{
            if(err){
                res.status(400).json({"errors":"error membaca data karyawan"})
            }
            else{
                let existingData = JSON.parse(data)
                let {users} = existingData
                let {name,password} = req.body
                let tempData = {name,password}
                let index = users.findIndex(item => item.name == tempData.name && item.password == tempData.password)
                users[index].isLogin = true
                let newData = {...existingData,users: users}
                fs.writeFile('data.json',JSON.stringify(newData),(err)=>{
                    if(err)
                    {
                        res.status(400).json({errors:"gagal login"})
                    }
                    else{
                        res.status(201).json({message:"berhasil login"})
                    }
                })
            }
        })
    }

    static siswa(req,res){
        fs.readFile('data.json',(err,data) =>{
            if(err){
                res.status(400).json({"errors":"error membaca data karyawan"})
            }
            else{
                let existingData = JSON.parse(data)
                let {users} = existingData
                let {name,kelas} = req.body
                let newStudent = {name,kelas}
                let indexTrainer = users.findIndex(item => item.role == 'trainer' && item.name == req.params.name)
                let indexAdmin = users.findIndex(item => item.role == 'admin')
                console.log(indexTrainer)
                if(indexAdmin == -1)
                {
                    res.status(201).json({message:"gagal menambahkan siswa, admin harus login"})
                }
                else
                {
                    if('students' in users[indexTrainer])
                    {
                        users[indexTrainer].students.push(newStudent)
                    }
                    else
                    {
                        users[indexTrainer].students = [newStudent]
                    }
                    let newData = {...existingData,users: users}
                    fs.writeFile('data.json',JSON.stringify(newData),(err)=>{
                        if(err)
                        {
                            res.status(400).json({errors:"gagal add siswa"})
                        }
                        else{
                            res.status(201).json({message:"berhasil add siswa"})
                        }
                    })
                }
            }
        })
    }
}
module.exports = userController