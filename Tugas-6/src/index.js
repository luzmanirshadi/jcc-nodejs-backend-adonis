import {sapa,convert,checkScore,filterData} from './lib/code'

var args = process.argv.slice(2)
var command = args[0]

switch (command) {
    case "sapa":console.log(sapa(args[1]));break;
    case "convert":console.log(convert(args[1],args[2],args[3]));break;
    case "checkScore":console.log(checkScore(args[1]));break;
    case "filterData":console.log(filterData(args[1]));break;
    default:break;
}