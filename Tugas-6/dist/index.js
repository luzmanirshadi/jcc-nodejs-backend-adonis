"use strict";

var _code = require("./lib/code");

var args = process.argv.slice(2);
var command = args[0];

switch (command) {
  case "sapa":
    console.log((0, _code.sapa)(args[1]));
    break;

  case "convert":
    console.log((0, _code.convert)(args[1], args[2], args[3]));
    break;

  case "checkScore":
    console.log((0, _code.checkScore)(args[1]));
    break;

  case "filterData":
    console.log((0, _code.filterData)(args[1]));
    break;

  default:
    break;
}