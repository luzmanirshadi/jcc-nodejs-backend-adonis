"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.filterData = exports.checkScore = exports.convert = exports.sapa = void 0;

var sapa = function sapa(nama) {
  return "halo selamat pagi ".concat(nama);
};

exports.sapa = sapa;

var convert = function convert(nama, domisili, umur) {
  var person = {
    nama: nama,
    domisili: domisili,
    umur: umur
  };
  return person;
};

exports.convert = convert;

var checkScore = function checkScore() {
  var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new String();
  var arr = data.split(",");
  console.log(arr);
  var arr1 = [];

  for (var i = 0; i < arr.length; i++) {
    arr1.push(arr[i].split(":"));
  }

  var name = arr1[0][1];
  var kelas = arr1[1][1];
  var score = arr1[2][1];
  var person = {
    name: name,
    "class": kelas,
    score: score
  };
  return person;
};

exports.checkScore = checkScore;
var data = [{
  name: "Ahmad",
  "class": "adonis"
}, {
  name: "Regi",
  "class": "laravel"
}, {
  name: "Bondra",
  "class": "adonis"
}, {
  name: "Iqbal",
  "class": "vuejs"
}, {
  name: "Putri",
  "class": "Laravel"
}];

var filterData = function filterData(kelas) {
  return data.filter(function (data) {
    return data["class"] == kelas;
  });
};

exports.filterData = filterData;