import {register,login,addSiswa} from './lib/code.js'

const args = process.argv.slice(2)
const command = args[0]

switch (command) {
    case "register":
        let newData = {}
        let [nama,password,role] = args[1].split(',')
        newData.nama = nama
        newData.password = password
        newData.role = role
        newData.isLogin = false
        if(role == "trainer")
        {
            newData.students = []
        }
        register(newData);
        break;
    case "login":
        let param = {}
        let [id,pass] = args[1].split(',')
        param.nama = id
        param.password = pass
        login(param);
        break;
    case "addSiswa":
        let [student,trainer] = args[1].split(',')
        addSiswa(student,trainer);
        break;
    default:
        break;
}