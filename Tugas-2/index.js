var global = require('./lib')

console.log("Tugas String")
//Output no-1
console.log("output no-1")
var first1 = global.no1.word
var second1 = global.no1.second
var third1 = global.no1.third
var fourth1 = global.no1.fourth
var fifth1 = global.no1.fifth
var sixth1 = global.no1.sixth
var seventh1 = global.no1.seventh
console.log(first1,second1,third1,fourth1,fifth1,sixth1,seventh1)
console.log("")

//Output no-2
console.log("output no-2")
var first2 = global.no2.firstWord
var second2 = global.no2.secondWord
var third2 = global.no2.thirdWord
var fourth2 = global.no2.fourthWord
var fifth2 = global.no2.fifthWord
var sixth2 = global.no2.sixthWord
var seventh2 = global.no2.seventhWord
var eight2 = global.no2.eighthWord
console.log("First word:",first2)
console.log("Second word:",second2)
console.log("Third word:",third2)
console.log("Fourth word:",fourth2)
console.log("Fifth word:",fifth2)
console.log("Sixth word:",sixth2)
console.log("Seventh word:",seventh2)
console.log("Eight word:",eight2)
console.log("")

//Output no-3
console.log("output no-3")
var first3 = global.no3.firstWord
var second3 = global.no3.secondWord
var third3 = global.no3.thirdWord
var fourth3 = global.no3.fourthWord
var fifth3 = global.no3.fifthWord
console.log('First Word: ' + first3); 
console.log('Second Word: ' + second3); 
console.log('Third Word: ' + third3); 
console.log('Fourth Word: ' + fourth3); 
console.log('Fifth Word: ' + fifth3);
console.log("")

//Output no-4
console.log("output no-4")
var first4 = global.no4.firstWord
var second4 = global.no4.secondWord
var third4 = global.no4.thirdWord
var fourth4 = global.no4.fourthWord
var fifth4 = global.no4.fifthWord
console.log('First Word: ' + first4 + ', with length: ' + first4.length)
console.log('Second Word: ' + second4 + ', with length: ' + second4.length)
console.log('Third Word: ' + third4 + ', with length: ' + third4.length)
console.log('Fourth Word: ' + fourth4 + ', with length: ' + fourth4.length)
console.log('Fifth Word: ' + fifth4 + ', with length: ' + fifth4.length)

console.log("")
console.log("")
console.log("Tugas Conditional")
//Tugas if-else
console.log("Tugas if-else")
var nama = ""
var peran = ""
function ifs(nama,peran)
{
    if(nama == '' && peran == '')
    {
        console.log("Nama harus diisi!")
    }
    else if(peran == '')
    {
        console.log("Halo",nama,"Pilih peranmu untuk memulai")
    }
    else if(peran == 'Penyihir')
    {
        console.log("Selamat datang di Dunia Werewolf,",nama)
        console.log("Halo Penyihir",nama,"kamu dapat melihat siapa yang menjadi werewolf!")
    }
    else if(peran == 'Guard')
    {
        console.log("Selamat datang di Dunia Werewolf,",nama)
        console.log("Halo Guard",nama,"kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }
    else if(peran == 'Werewolf')
    {
        console.log("Selamat datang di Dunia Werewolf,",nama)
        console.log("Halo Werewolf",nama,"Kamu akan memakan mangsa setiap malam!")
    }
}
console.log("Output untuk Input nama = '' dan peran = ''")
ifs(nama,peran)
console.log("")
console.log("Output untuk Input nama = 'John' dan peran = ''")
ifs("John",peran)
console.log("")
console.log("Output untuk Input nama = 'Jane' dan peran 'Penyihir'")
ifs("Jane","Penyihir")
console.log("")
console.log("Output untuk Input nama = 'Jenita' dan peran = 'Guard'")
ifs("Jenita","Guard")
console.log("")
console.log("Output untuk Input nama = 'Junaedi' dan peran = 'Werewolf'")
ifs("Jenita","Werewolf")
console.log("")
console.log("")

//Tugas Switch-case
console.log("Tugas switch-case")
var tanggal = 21
var bulan = 1
var tahun = 1945
function swc(tanggal,bulan,tahun)
{
    switch (bulan){
        case 1: {console.log(tanggal,"Januari",tahun);break}
        case 2: {console.log(tanggal,"Februari",tahun);break}
        case 3: {console.log(tanggal,"Maret",tahun);break}
        case 4: {console.log(tanggal,"April",tahun);break}
        case 5: {console.log(tanggal,"Mei",tahun);break}
        case 6: {console.log(tanggal,"Juni",tahun);break}
        case 7: {console.log(tanggal,"Juli",tahun);break}
        case 8: {console.log(tanggal,"Agustus",tahun);break}
        case 9: {console.log(tanggal,"September",tahun);break}
        case 10: {console.log(tanggal,"Oktober",tahun);break}
        case 11: {console.log(tanggal,"November",tahun);break}
        case 12: {console.log(tanggal,"Desember",tahun);break}
        default: {console.log("Angka bulan tidak sesuai")}
    }
}
swc(tanggal,bulan,tahun)