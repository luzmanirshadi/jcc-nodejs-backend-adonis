var sentence = 'wow JavaScript is so cool' 

var firstWord = sentence.substr(0,3)
var secondWord = sentence.substr(4,10) 
var thirdWord = sentence.substr(15,2)
var fourthWord = sentence.substr(18,2)
var fifthWord = sentence.substr(21)
var sentenceLength = sentence.length

module.exports = {firstWord,secondWord,thirdWord,fourthWord,fifthWord,sentenceLength}