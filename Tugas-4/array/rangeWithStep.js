function rws(num1,num2,step){
    var arr = []
    if(num1 == null || num2 == null || step == null)
    {
        arr = arr
    }
    else {
        num1 = parseInt(num1)
        num2 = parseInt(num2)
        step = parseInt(step)
        if(num1 > num2)
        {
            while(num1 >= num2)
            {
                arr.push(num1)
                num1 -= step
            }
        }
        else
        {
            while(num1 <= num2)
            {
                arr.push(num1)
                num1 += step
            }
        }
    }
    console.log(arr)
}
module.exports = {rws:rws}