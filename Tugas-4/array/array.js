var range = require('./range')
var rws = require('./rangeWithStep')
var sum = require('./sum')
var dh = require('./dataHandling')
var bk = require('./balikKata')

var args = process.argv
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

switch(args[2])
{
    case "range" : range.range(args[3],args[4]); break;
    case "rangeWithStep" : rws.rws(args[3],args[4],args[5]); break;
    case "sum": sum.sum(args[3],args[4],args[5]); break;
    case "dataHandling": dh.dh(input); break;
    case "balikKata": bk.bk(args[3]); break;
    default:break;
}