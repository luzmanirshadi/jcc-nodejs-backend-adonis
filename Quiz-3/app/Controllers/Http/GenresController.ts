import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import CreateGenreValidator from 'App/Validators/CreateGenreValidator'

export default class GenresController {
    public async index({response}:HttpContextContract){
        let genres = await Database.from('genres').select('*')
        response.ok({message : 'success get genres', data: genres})
    }

    public async store({request,response}:HttpContextContract){
        await request.validate(CreateGenreValidator)
        await Database.table('genres').returning('id').insert({
            name: request.input('name'),
        })
        response.created({message: 'genre created!'})
    }

    public async show({params,response}:HttpContextContract){
        let genre = await Database.from('genres').where('id',params.id).select('*')
        let movie = await Database.from('movies').innerJoin('genres','genres.id','movies.genre_id').where('genres.id',params.id).select('movies.*')
        let data = {
            id: genre[0].id,
            name: genre[0].name,
            movie: movie
        }
        return response.ok({data})
    }

    public async update({params,request,response}:HttpContextContract){
        let id = params.id
        await request.validate(CreateGenreValidator)
        await Database.from('genres').where('id',id).update({
            name: request.input('name')
        })
        response.ok({message : 'success update genre'})
    }

    public async destroy({params,response}:HttpContextContract){
        await Database.from('genres').where('id',params.id).delete()
        response.ok({message : 'success delete genre'})
    }
}
