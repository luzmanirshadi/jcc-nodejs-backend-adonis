import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import CreateMovieValidator from 'App/Validators/CreateMovieValidator'

export default class MoviesController {
    public async index({response}:HttpContextContract){
        let movies = await Database.from('movies').select('*')
        response.ok({message : 'success get movies', data: movies})
    }

    public async store({request,response}:HttpContextContract){
        await request.validate(CreateMovieValidator)
        await Database.table('movies').returning('id').insert({
            title: request.input('title'),
            resume: request.input('resume'),
            release_date: request.input('release_date'),
            genre_id: request.input('genre_id')
        })
        response.created({message: 'movie created!'})
    }

    public async show({params,response}:HttpContextContract){
        let movie = await Database.from('movies').where('id',params.id).select('*').first()
        let genre = await Database.from('genres').where('id',movie.genre_id).select('*').first()
        return response.ok({
            id: movie.id,
            title: movie.title,
            release_date: movie.release_date,
            genre: genre.name
        })
    }

    public async update({params,request,response}:HttpContextContract){
        let id = params.id
        await request.validate(CreateMovieValidator)
        await Database.from('movies').where('id',id).update({
            title: request.input('title'),
            resume: request.input('resume'),
            release_date: request.input('release_date')
        })
        response.ok({message : 'success update movie'})
    }

    public async destroy({params,response}:HttpContextContract){
        await Database.from('movies').where('id',params.id).delete()
        response.ok({message : 'success delete movie'})
    }
}
