function ato(arr){
    if (arr == null)
    {
        console.log("")
    }
    else
    {
        var date = new Date()
        var thisYear = date.getFullYear()
        var person = {}
        for(var i = 0;i < arr.length;i++)
        {
            person.firstname = arr[i][0]
            person.lastname = arr[i][1]
            person.gender = arr[i][2]
            var age = thisYear-arr[i][3]
            if(isNaN(age) || age < 0)
            {
                person.age = "Invalid Birth Year"
            }
            else
            {
                person.age = age
            }
            var name = person.firstname+" "+person.lastname
            console.log((i+1),". ",name,":",person)
        }
    }
}
module.exports = {ato:ato}
//ato([["abduh","muhamad","male"],["luzman","irshadi","male","1994"]])