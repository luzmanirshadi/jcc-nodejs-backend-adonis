function st(memberID,money){
    var sepatu = {name:"Stacattu",price:1500000}
    var baju = {name:"Zoro",price:500000}
    var baju1 = {name:"H&N",price:250000}
    var sweater = {name:"Sweater Uniklooh",price:175000}
    var hpCase = {name:"Handphone Case",price:50000}
    var arrItem = [sepatu,baju,baju1,sweater,hpCase]
    if(memberID == null || memberID == '')
    {
        console.log("Mohon maaf, toko X hanya berlaku untuk member saja")
    }
    else if(money < 50000)
    {
        console.log("Mohon maaf, uang tidak cukup")
    }
    else
    {
        var itemList = []
        var i = 0
        var j = 0
        while(i < arrItem.length)
        {
            var harga = arrItem[i].price
            var item = arrItem[i].name
            if(money >= harga)
            {
                money -= harga
                itemList.push(item)
            }
            else if(money < 50000)
            {
                break;
            }
            i++
        }
        var member = {memberId: memberID,money:money,listPurchased:itemList}
        console.log(member)
    }
}
module.exports = {st:st}