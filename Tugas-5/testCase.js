var ato = require('./arrayToObject')
var na = require('./naikAngkot')
var st = require('./shoppingTime')
var nt = require('./nilaiTertinggi')

//Test Case arrayToObject
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
console.log("Output Test Case ArrayToObject")
ato.ato(people)
ato.ato(people2)
console.log()
console.log()

//Test Case ShoppingTime
console.log("Output Test Case ShoppingTime")
st.st('1820RzKrnWn08', 2475000)
st.st('', 2475000)
st.st('1820RzKrnWn08', 15000)
st.st()
console.log()
console.log()

//Test Case NaikAngkot
console.log("Output Test Case NaikAngkot")
na.na([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']])
console.log()
console.log()

//Test Case NilaiTertinggi
console.log("Output Test Case NilaiTertinggi")
var data = [
    {
      name: 'Asep',
      score: 90,
      class: 'adonis'
    },
    {
      name: 'Ahmad',
      score: 85,
      class: 'vuejs'
    },
    {
      name: 'Regi',
      score: 74,
      class: 'adonis'
    },
    {
      name: 'Afrida',
      score: 78,
      class: 'reactjs'
    }
  ]
nt.nt(data)