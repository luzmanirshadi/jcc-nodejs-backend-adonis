var loopfor = require('./for')
var loopwhile = require('./while')
var pp = require('./persegiPanjang')
var tangga = require('./tangga')
var catur = require('./catur')

var args = process.argv

switch(args[2])
{
    case "while" : loopwhile.loopwhile(); break;
    case "for" : loopfor.loopfor(); break;
    case "persegiPanjang": pp.pp(args[3],args[4]); break;
    case "tangga" : tangga.tangga(args[3]); break;
    case "catur" : catur.catur(args[3]); break;
    default:break;
}